package seniordesign.chromaticanalysis;

import org.jfugue.*;


/**
 * Implements the Direct-mapping algorithm for producing music from images.
 * 
 * @author Eric OBrien ericob@seas.upenn.edu
 * @author user
 * @author user
 *
 */

public class BrickToMusic {
	
	public static String indToNote(double cindex) {
		int ci = (int)(cindex*10);
		switch (ci) {
		case 10:
			return "B";
		case 11:
			return "F#";
		case 12:
			return "F";
		case 13:
			return "E";
		case 14:
			return "D";
		case 15:
			return "C";
		case 16:
			return "A#";
		case 17:
			return "G";
		case 18:
			return "A";
		case 19:
			return "C#";
		case 20:
			return "C";
		case 21:
			return "C";
		default:
			System.err.println("ERROR AT "+ cindex); return "B";
		}
	}
	
	/**
	 * Composes a musical piece through DirectMapping of the pixels 
	 * in the image.
	 * Takes average 
	 * @param pixels: the pixels of the input image
	 * @return a pattern containing the song
	 */
	public static Pattern convertToPattern(CBrick cb){
		StringBuffer sb = new StringBuffer();
		// Given the overall index of the brick, pick a chord to play
		double mainind = cb.getChroma();
		sb.append(indToNote(mainind) + "5majq ");
		//for(Chroma c: cb.getChromaList()){
		//		sb.append(" "+indToNote(c.getCI())+"i");
		//}
		Pattern dm = new Pattern(sb.toString());
		return dm;
	}

}
