package seniordesign.chromaticanalysis;

import seniordesign.Pixel;

/**
 * The ColorMatch class will be used to apply the correct color name to the RGB value of a pixel.
 */

public class ColorMatch {

	private int[][] colors;
	private String[] colorNames;
	
	// This method takes a RGB value and finds the mathematically closest color (out of our 12 choices)
	protected String colorName(int rgb){
		int r = Pixel.getRed(rgb);
		int g = Pixel.getGreen(rgb);
		int b = Pixel.getBlue(rgb);
		// now compare the r, g, and b values to each of our r, g, and b values, respectively, of the 12 color values, and decide which is mathematically the closest
		int diff = 0;
		String matchingColor = "";
		for (int i = 0; i < colors.length; i++){
			int tempDiff = 0;
			tempDiff = tempDiff + Math.abs(r - colors[i][0]);
			tempDiff = tempDiff + Math.abs(g - colors[i][1]);
			tempDiff = tempDiff + Math.abs(b - colors[i][2]);
			if (tempDiff < diff){
				diff = tempDiff;
				matchingColor = colorNames[i];
			}
		}
		return matchingColor;
	}
	
	/**
	 * Instantiates a new ColorMatch object and loads the R,G,B values 
	 * for each of our 12 colors, and our 12 color names
	 */
	protected ColorMatch(){
		colors = new int[12][3];
		colorNames = new String[12];
		colorNames[0] = "white";
		colorNames[1] = "pink";
		colorNames[2] = "red";
		colorNames[3] = "orange";
		colorNames[4] = "yellow";
		colorNames[5] = "green";
		colorNames[6] = "turquoise";
		colorNames[7] = "blue";
		colorNames[8] = "violet";
		colorNames[9] = "brown";
		colorNames[10] = "black";
		colorNames[11] = "grey";
		// RGB values from http://kb.iu.edu/data/aetf.html
		colors[0][0] = 255;
		colors[0][1] = 255;
		colors[0][2] = 255;
		colors[1][0] = 255; 
		colors[1][1] = 192;
		colors[1][2] = 203;
		colors[2][0] = 255;
		colors[2][1] = 0;
		colors[2][2] = 0;
		colors[3][0] = 255;
		colors[3][1] = 165;
		colors[3][2] = 0;
		colors[4][0] = 255;
		colors[4][1] = 255;
		colors[4][2] = 0;
		colors[5][0] = 0;
		colors[5][1] = 128;
		colors[5][2] = 0;
		colors[6][0] = 64;
		colors[6][1] = 224;
		colors[6][2] = 208;
		colors[7][0] = 0;
		colors[7][1] = 0;
		colors[7][2] = 255;
		colors[8][0] = 238;
		colors[8][1] = 120;
		colors[8][2] = 238;
		colors[9][0] = 165;
		colors[9][1] = 42;
		colors[9][2] = 42;
		colors[10][0] = 0;
		colors[10][1] = 0;
		colors[10][2] = 0;
		colors[11][0] = 128;
		colors[11][1] = 128;
		colors[11][2] = 128;
	}
	
}
