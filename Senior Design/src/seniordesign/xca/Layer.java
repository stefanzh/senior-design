package seniordesign.xca;

import seniordesign.Pair;
import seniordesign.Pixel;

public class Layer {
	
	private int[][] pixels;
	private double[][] density;
	private Pair<Integer, Integer> range;
	private int index;
	
	/**
	 * Constructor for a Layer
	 */
	public Layer(int[][] pixels, double[][] density, Pair<Integer, Integer> range, int index){
		this.pixels = pixels;
		this.density = density;
		this.range = range;
		this.index = index;
	}
	
	/**
	 * Finds the maximum density in a density array.
	 * 
	 * @return the largest density in the array
	 */
	private double maxDensity(){
		double maxDensity = 0;
		for(int i = 0; i < density.length; i++){
			for(int j = 0; j < density[0].length; j++){
				maxDensity = Math.max(maxDensity, density[i][j]);
			}
		}
		return maxDensity;
	}
	
	public String toString(){
		return index+": "+maxDensity()+ " ("+range.getKey()+", "+range.getValue()+")";
	}
	
	/**
	 * Finds the average color of the section specified by the layer and
	 * the area of the section.
	 * 
	 * @param img - reference to the original image
	 * @return pair of the value of the average pixel and the area of the layer
	 */
	protected Pair<Integer, Integer> avgColor(int[][] img){
		double avgRed = 0;
		double avgGreen = 0;
		double avgBlue = 0;
		int totalArea = 0;
		for(int i = 0; i < pixels.length; i++){
			int r = 0;
			int g = 0;
			int b = 0;
			int area = 0;
			for(int j = 0; j < pixels[0].length; j++){
				if(!Pixel.isWhite(pixels[i][j])){
					r += Pixel.getRed(img[i][j]);
					g += Pixel.getGreen(img[i][j]);
					b += Pixel.getBlue(img[i][j]);
					area++;
				}
			}
			avgRed += (area > 0) ? (r/area) : 0;
			avgGreen += (area > 0) ? (g/area) : 0;
			avgBlue += (area > 0) ? (b/area) : 0;
			totalArea += area;
		}
		avgRed = avgRed/pixels.length;
		avgGreen = avgGreen/pixels.length;
		avgBlue = avgBlue/pixels.length;
		
		int pixel = Pixel.create((int)avgRed, (int)avgGreen, (int)avgBlue);
		return new Pair<Integer, Integer>(pixel, totalArea);
	}

}
