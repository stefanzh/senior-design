package seniordesign.xca;

import seniordesign.chromaticanalysis.Chroma;

public class ChromaticVertex implements Comparable<ChromaticVertex>{
	
	private Chroma c;
	private int area;
	
	public ChromaticVertex(Chroma c, int area){
		this.c = c;
		this.area = area;
	}
	
	public Chroma getChroma(){
		return this.c;
	}
	
	public int getArea(){
		return this.area;
	}
	
	public int compareTo(ChromaticVertex v2){
		if (this.area == v2.getArea()) return 0;
		return this.area > v2.getArea() ? -1 : 1;
	}
	
	public String toString(){
		return c.toString() + " #" + area;
	}
	
	/**
	 * Combines two Chromatic Vertices.
	 * 
	 * @param ocv - the other Chromatic Vertex
	 * @return the combined Chromatic Vertices
	 */
	public ChromaticVertex combine(ChromaticVertex ocv){
		this.c = c.combine(ocv.c);
		this.area = area + ocv.area;
		return this;
	}
	
	/**
	 * Decides on the mood of the piece
	 * 
	 * @param ci
	 * @param cp
	 * @return
	 */
	private int[] chromaToMood(double ci, ChordProgression cp){
		if(ci <= 1.0) return cp.alternative;
		if(ci <= 1.1) return cp.canon;
		if(ci <= 1.2) return cp.cliche;
		if(ci <= 1.3) return cp.cliche2;
		if(ci <= 1.4) return cp.endless;
		if(ci <= 1.5) return cp.energetic;
		if(ci <= 1.6) return cp.memories;
		if(ci <= 1.7) return cp.rebellious;
		if(ci <= 1.8) return cp.twelvebarblues;
		if(ci <= 1.9) return cp.wistful;
		if(ci <= 2.0) return cp.grungy;
		if(ci <= 2.1) return cp.sad;
		return cp.twelvebarblues;
	}

	/**
	 * Converts the given layer to a sequence of chords.
	 * 
	 * @param img - a reference to the original pixel image
	 * @return - a string of chords
	 */
	public String toChord() {
		ChordProgression cp = new ChordProgression();
		int[] mood = this.chromaToMood(c.getCI(), cp);
		String[][] relatedProgressions = cp.relatedProgressions(mood, "C", "maj");
		String[] chords = relatedProgressions[0];
		String t = new String();
		for(int i = 0; i < chords.length; i++){
			t += " " + chords[i];
		}
		return t;
	}

}
