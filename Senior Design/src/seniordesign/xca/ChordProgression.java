package seniordesign.xca;

/**
 * Chord Progression class
 * 
 * Inspired by Alex Robinson http://alextrob.net/
 * who was inspired by a picture on the Internet http://imgur.com/wTpnp
 * 
 * Adapted to Java by Stefan Zhelyazkov <stz@seas.upenn.edu>
 *
 */
public class ChordProgression {
	
	/* Basic Chord Progressions a.k.a. "Moods" */
	public int[] alternative = {6, 4, 1, 5};
	public int[] canon = {1, 5, 6, 3, 4, 1, 4, 5};
	public int[] cliche = {1, 5, 6, 4};
	public int[] cliche2 = {1, 6, 3, 7};
	public int[] creepy = {1, 6, 4, 5};
	public int[] creepy2 = {1, 6, 2, 5};
	public int[] endless = {1, 6, 2, 4};
	public int[] energetic = {1, 3, 4, 6};
	public int[] grungy = {1, 4, 3, 6};
	public int[] memories = {1, 4, 1, 5};
	public int[] rebellious = {4, 1, 4, 5};
	public int[] sad = {1, 4, 5, 5};
	public int[] simple = {1, 4};
	public int[] simple2 = {1, 5};
	public int[] twelvebarblues = {1, 1, 1, 1, 4, 4, 1, 1, 5, 4, 1, 5};
	public int[] wistful = {1, 1, 4, 6};
	
	/* Notes with sharps */
	private String[] notesWithSharps = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
	
	/**
	 * Computes the mixture of the notes from the two spinning circles.
	 * 
	 * @param key - starting note
	 * @param majMin - maj/min/lygian/phrygian
	 * @return the combination of notes
	 */
	// Usage: getChords("A#", "min")
	public String[] getChords(String key, String majMin){
		String[] notesStartingAtKey = notesWithStartPoint(key, notesWithSharps);
		int[] steps = {};
		String[] majMinArr = {};
		
		if(majMin.equals("maj")){
			steps = new int[]{0, 2, 4, 5, 7, 9, 11};
			majMinArr = new String[]{"maj", "min", "min", "maj", "maj", "min", "dim"};
		}
		else if (majMin.equals("min")){
			steps = new int[]{0, 2, 3, 5, 7, 8, 10};
			majMinArr = new String[]{"min", "dim", "maj", "min", "min", "maj", "maj"};
		}
		else if (majMin.equals("lydian")){
			steps = new int[]{0, 2, 4, 6, 7, 9, 11};
			majMinArr = new String[]{"maj", "maj", "min", "dim", "maj", "min", "min"};
		}
		else if (majMin.equals("phrygian")){
			steps = new int[]{0, 1, 3, 5, 7, 8, 10};
			majMinArr = new String[]{"min", "maj", "maj", "min", "dim", "maj", "min"};
		}
		else {
			System.err.println("Incorrect majMin property: either maj/min/lydian/phrygian");
		}
		
		String[] notesInKey = new String[steps.length];
		for(int i = 0; i < steps.length; i++){
			int thisStep = steps[i];
			String thisNote = notesStartingAtKey[thisStep];
			String thisMajMin = majMinArr[i];
			notesInKey[i] = thisNote + thisMajMin;
		}
		return notesInKey;
	}

	/**
	 * Creates the spinning wheel for chords.
	 * 
	 * @param note - starting note
	 * @param notes - the array of notes
	 * @return - the wrapped around new array of notes, i.e. the inner circle
	 * of chords
	 */
	private String[] notesWithStartPoint(String note, String[] notes) {
		int index = 0;
		for(int i = 0; i < notes.length; i++){
			if(notes[i].equals(note)){
				index = i;
				break;
			}
		}
		String[] newNotes = new String[notes.length];
		for(int i = 0; i < notes.length; i++){
			if (index >= notes.length)
				index = 0;
			newNotes[i] = notes[index];
			index++;
		}
		return newNotes;
	}
	
	/**
	 * Computes the main progression for the given parameters.
	 * Based on the algorithm suggested by Alex Robinson
	 * 
	 * @param mood - one of the arrays defined in the beginning of the class
	 * @param key - the starting key
	 * @param natSharp - 'n' or 's' for natural or sharp
	 * @param majMin - "maj"/"min"/"lydian"/"phrygian"
	 */
	public String[] mainProgression(int[] mood, String key, boolean sharp, String majMin){
		if(!key.equals("B") && !key.equals("E") && sharp){
			key = key + "#";
		}
		
		String[] chordsInKey = this.getChords(key, majMin);
		String[] progression = new String[mood.length];
		for(int i = 0; i < mood.length; i++){
			progression[i] = chordsInKey[mood[i]-1];
		}
		return progression;
	}
	
	/**
	 * Computes the circle of Fifths for the given notes and the majMin value.
	 * 
	 * @param notes - provided notes
	 * @param majMin - maj/min/lygian/phrygian
	 * @return a 2D array of notes
	 */
	private String[][] getCircleOfFifths(String[] notes, String majMin){
		String[] outerCircle = new String[notes.length];
		String[] innerCircle = new String[0];
		if(notes.length != 12) {
			System.err.println("Something with the length of notes is wrong!");
			return new String[][]{outerCircle, innerCircle};
		}
		
		outerCircle[0] = notes[0];
		for(int i = 1; i < notes.length; i++){
			// Get the current note + 7 positions. Current note will be the last item in outerCircle[]
			// outerCircle[i] is the easiest way to find the most recent item added to the array 
			String[] notesStartingAtCurrentNote = notesWithStartPoint(outerCircle[i-1], notes);
			outerCircle[i] = notesStartingAtCurrentNote[7];
		}
		
		// generate the "inner circle" - just a slight "rotation" of the outer circle
		// rotates differently depending on whether major or minor 
		if(majMin.equals("maj"))
			innerCircle = notesWithStartPoint(outerCircle[3], outerCircle);
		if(majMin.equals("min"))
			innerCircle = notesWithStartPoint(outerCircle[9], outerCircle);
		if(majMin.equals("lydian"))
			innerCircle = notesWithStartPoint(outerCircle[4], outerCircle);
		if(majMin.equals("phrygian"))
			innerCircle = notesWithStartPoint(outerCircle[3], outerCircle);
		
		return new String[][]{outerCircle, innerCircle};
	}
	
	/**
	 * Returns the related starting keys for the given notes
	 * 
	 * @param notes - the notes available
	 * @param majMin - maj/min/lygian/phrygian
	 * @return a 2D array of related keys
	 */
	private String[][] getRelatedKeys(String[] notes, String majMin){
		String[][] circleOfFifths = this.getCircleOfFifths(notes, majMin);
		String[][] relatedKeys = new String[3][2];
		
		// Get the one opposite and the two either side.
		if(majMin.equals("maj")){
			relatedKeys[0] = new String[]{circleOfFifths[1][0], "min"};
			relatedKeys[1] = new String[]{circleOfFifths[0][circleOfFifths[1].length - 1], "maj"};
			relatedKeys[2] = new String[]{circleOfFifths[0][1], "maj"};
		}
		if(majMin.equals("min")){
			relatedKeys[0] = new String[]{circleOfFifths[1][0], "maj"};
			relatedKeys[1] = new String[]{circleOfFifths[0][circleOfFifths[1].length - 1], "min"};
			relatedKeys[2] = new String[]{circleOfFifths[0][1], "min"};
		}
		if(majMin.equals("lydian")){
			relatedKeys[0] = new String[]{circleOfFifths[1][0], "min"};
			relatedKeys[1] = new String[]{circleOfFifths[0][circleOfFifths[1].length - 1], "lydian"};
			relatedKeys[2] = new String[]{circleOfFifths[0][1], "lydian"};
		}
		if(majMin.equals("phrygian")){
			relatedKeys[0] = new String[]{circleOfFifths[1][0], "min"};
			relatedKeys[1] = new String[]{circleOfFifths[0][circleOfFifths[1].length - 1], "phrygian"};
			relatedKeys[2] = new String[]{circleOfFifths[0][1], "phrygian"};
		}
		
		return relatedKeys;
		
	}
	
	/**
	 * Computes and returns an array of the related chord progressions for the given mood, 
	 * starting key and "major/minor" value. The main progression is included at index 0
	 * 
	 * @param mood - the mood
	 * @param key - the starting key
	 * @param majMin - maj/min/lygian/phrygian
	 * @return the array of related chord progressions
	 */
	public String[][] relatedProgressions(int[] mood, String key, String majMin){
		String[][] relatedKeys = getRelatedKeys(notesWithStartPoint(key, notesWithSharps), majMin);
		String[][] relatedKeysChords = new String[relatedKeys.length][0];
		for(int i = 0; i < relatedKeys.length; i++){
			relatedKeysChords[i] = this.getChords(relatedKeys[i][0], relatedKeys[i][1]);
		}
		
		String[][] relatedChordProgressions = new String[relatedKeysChords.length+1][0];
		relatedChordProgressions[0] = this.mainProgression(mood, key, false, majMin);
		for(int i = 0; i < relatedKeysChords.length; i++){
			String[] tempArray = new String[mood.length];
			for(int j = 0; j < mood.length; j++){
				tempArray[j] = relatedKeysChords[i][mood[j]-1];
			}
			relatedChordProgressions[i+1] = tempArray;
		}
		
		return relatedChordProgressions;
	}

}
