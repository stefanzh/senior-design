package seniordesign.chromaticanalysis;

import java.util.LinkedList;


/**
 * Implementation of the Chromatic Brick.
 * 
 * @author Stefan Zhelyazkov stz@seas.upenn.edu
 * @author user
 * @author user
 *
 */
public class CBrick {
	
	private double cindex;
	private int dur;
	private LinkedList<Chroma> brick;
	private final double CI_THRESH = 0.05;
	private final double D_THRESH = 10;
	
	/**
	 * Constructor for a new Chromatic Brick.
	 */
	public CBrick(){
		this.cindex = 0;
		this.dur = 0;
		this.brick = new LinkedList<Chroma>();
	}
	
	/**
	 * Returns the chromatic index of the brick.
	 * @return the chromatic index of the brick.
	 */
	public double getChroma(){
		return cindex;
	}
	
	/**
	 * Returns the linked list of chroma.
	 * @return the linked list of chroma.
	 */
	public LinkedList<Chroma> getChromaList(){
		return brick;
	}
	
	/**
	 * Gets the duration of the brick.
	 * @return the duration of the brick.
	 */
	public int getDuration(){
		return dur;
	}
	
	/**
	 * If the chroma is within the accepted boundaries for the chromatic brick,
	 * the new pixel is added to the brick. If it is above the thresholds, the brick
	 * is considered completed.
	 * 
	 * @param c - the chroma to be added
	 * @return true if chroma added to the brick, false otherwise
	 */
	public boolean add(Chroma c){
		if(cindex == 0 || (Math.abs(cindex - c.getCI()) < CI_THRESH && dur <= D_THRESH)){
			double sum = cindex * brick.size() + c.getCI();
			brick.add(c);
			dur++;
			cindex = sum/dur;
			return true;
		}
		return false;
	}
	
	/**
	 * String representation of the brick.
	 */
	public String toString(){
		return brick.toString();
	}
	
}
