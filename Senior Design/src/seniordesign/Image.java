package seniordesign;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * Class that handles images.
 * 
 * @author Stefan Zhelyazkov stz@seas.upenn.edu
 * @author user 
 * @author user
 *
 */
public class Image {
	
	/* Holds the 2D array of the pixels */
	private int[][] pixels;
	
	/* Average RGB values */
	private int avgR;
	private int avgG;
	private int avgB;
	
	/**
	 * Reads an image:
	 * Constructs the 2-d pixel array from it.
	 * Computes the average RGB values.
	 * 
	 * @param filename: the image file
	 */
	public Image(String filename){
		if(filename == null) return;
		try {
			BufferedImage img = ImageIO.read(new File(filename));
			int h = img.getHeight();
			int w = img.getWidth();
			int[][] pixels = new int[w][h];
			for(int i = 0; i < w; i++){
				int ar = 0;
				int ag = 0;
				int ab = 0;
				for(int j = 0; j < h; j++){
					int pixel = img.getRGB(i, j);
					pixels[i][j] = pixel;
					ar += Pixel.getRed(pixel);
					ag += Pixel.getGreen(pixel);
					ab += Pixel.getBlue(pixel);
				}
				avgR += ar/h;
				avgG += ag/h;
				avgB += ab/h;
			}
			avgR = avgR/w;
			avgG = avgG/w;
			avgB = avgB/w;
			this.pixels = pixels;
		} catch (IOException ioe){
			System.out.println(ioe.getMessage());
		}
	}
	
	/**
	 * Returns the 2D array with the pixels.
	 * @return
	 */
	public int[][] getPixels(){
		return this.pixels;
	}
	
	/**
	 * Return the average Red value of the image
	 */
	public int avgRed(){
		return this.avgR;
	}
	
	/**
	 * Return the average Green value of the image
	 */
	public int avgGreen(){
		return this.avgG;
	}
	
	/**
	 * Return the average Blue value of the image
	 */
	public int avgBlue(){
		return this.avgB;
	}
	
	/**
	 * Return the average pixel of the image
	 */
	public int avgPixel(){
		return Pixel.create(avgR, avgG, avgB);
	}
	
	/**
	 * Computes a new pixel from the original grid.
	 * 
	 * @param i - i position of the new pixel
	 * @param j - j position of the new pixel
	 * @param step - the range of computation
	 * @return the new pixel
	 */
	private int computePixel(int i, int j, double step){
		int sumR = 0;
		int sumG = 0;
		int sumB = 0;
		int count = 0;
		int xstart = (int)(i*step);
		int ystart = (int)(j*step);
		int xrange = (int)((i+1)*step);
		int yrange = (int)((j+1)*step);
		for(int x = xstart; x <= xrange; x++){
			for(int y = ystart; j <= yrange; j++){
				sumR += Pixel.getRed(pixels[x][y]);
				sumG += Pixel.getGreen(pixels[x][y]);
				sumB += Pixel.getBlue(pixels[x][y]);
				count++;
			}
		}
		sumR = sumR/count;
		sumG = sumG/count;
		sumB = sumB/count;
		return Pixel.create(sumR, sumG, sumB);
	}
	
	/**
	 * Scales the image by the resizing factor provided in the constructor.
	 * Keeps the proportions of the image.
	 * @param input - the input image grid
	 * @return the resized pixel grid
	 */
	public int[][] scale(double factor){
		int newWidth = (int)(pixels.length * factor);
		int newHeight = (int)(pixels[0].length * factor);
		int[][] output = new int[newWidth][newHeight];
		double step = 1/factor;
		for(int i = 0; i < newWidth; i++){
			for(int j = 0; j < newHeight; j++){
				output[i][j] = computePixel(i, j, step);
			}
		}
		pixels = output;
		return pixels;
	}
	
	/**
	 * Scales an image by preserving its proportions and restricting it
	 * to a maximum width and height.
	 * @param input - the input image grid
	 * @param maxWidth - maximum width of the result image
	 * @param maxHeight - maximum height of the result image
	 * @return the resized pixel grid
	 */
	public int[][] scale(int maxWidth, int maxHeight){
		if(pixels.length <= maxWidth && pixels[0].length <= maxHeight)
			return pixels;
		double w = pixels.length;
		double h = pixels[0].length;
		double ratio = maxWidth/w;
		if(h*ratio > maxHeight)
			ratio = maxHeight/h;
		return scale(ratio);
	}
	
	/**
	 * Converts a pixel map into a Buffered Image instance.
	 * @param input - the pixels grid
	 * @return the new buffered image
	 */
	public static BufferedImage makeImage(int[][] input){
		int w = input.length;
		int h = input[0].length;
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				bi.setRGB(i, j, input[i][j]);
			}
		}
		return bi;
	}
	
	/**
	 * Writes a buffered image to a specified file.
	 * @param bi - the buffered image
	 * @param filename - a String with the file name 
	 */
	public static void write(BufferedImage bi, String fileName){
		try{
			ImageIO.write(bi, "jpeg", new File(fileName));
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

}
