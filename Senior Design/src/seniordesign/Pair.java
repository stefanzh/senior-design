package seniordesign;

/**
 * Implementation of the basic data structure Pair.
 * @author Stefan Zhelyazkov <stz@seas.upenn.edu>
 * @author user
 * @author user
 *
 * @param <K> - the key type
 * @param <V> - the value type
 */
public class Pair<K,V extends Comparable<V>> implements Comparable<Pair<K,V>> {
	
	/* key and value variables */
	private final K key;
	private final V value;

	/**
	 * Class constructor. 
	 */
	public Pair(K key, V value) {
		this.key = key;
	    this.value = value;
	}
	
	/**
	 * Getters
	 */
	public K getKey() { return key; }
	public V getValue() { return value; }

	@Override
	public int hashCode() { return key.hashCode() ^ value.hashCode(); }

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
	    if (!(o instanceof Pair)) return false;
	    @SuppressWarnings("unchecked")
		Pair<K,V> pairo = (Pair<K,V>) o;
	    return this.key.equals(pairo.getKey()) && this.value.equals(pairo.getValue());
	}

	/**
	 * This comparator compares the values of the Pairs.
	 */
	@Override
	public int compareTo(Pair<K,V> o) {
		return this.value.compareTo(o.value);
	}
	
	@Override
	public String toString(){
		return "(" + this.key + ", " + this.value + ")";
	}

}
