package seniordesign.xca;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import seniordesign.Image;
import seniordesign.Pair;
import seniordesign.Pixel;

public class HistogramSlicer {
	
	private static final int BLOCK_SIZE = 20;
	private static final double DENSITY_THRESH = 0.334;
	
	/**
	 * Slices the RGB histogram of an image and creates a new pixel array
	 * with the selected values.
	 * 
	 * @param img - original image 
	 * @param channel - color channel: either 'r', 'g', or 'b'
	 * @param height - minimum threshold height of the color histogram column 
	 * @return an array with the RGB slice.
	 */
	public static int[][] slice(int[][] img, char channel, int height){
		if(height < 0 || height > 255)
			System.err.println("Invalid slice height provided.");
		int[][] slice = new int[img.length][img[0].length];
		for(int i = 0; i < img.length; i++){
			for(int j = 0; j < img[0].length; j++){
				switch (channel){
				case 'r': 
					if(Pixel.getRed(img[i][j]) >= height){
						slice[i][j] = Pixel.create(Pixel.getRed(img[i][j]), 0, 0);
					} break;
				case 'g':
					if(Pixel.getGreen(img[i][j]) >= height){
						slice[i][j] = Pixel.create(0, Pixel.getGreen(img[i][j]), 0);
					} break;
				case 'b':
					if(Pixel.getBlue(img[i][j]) >= height){
						slice[i][j] = Pixel.create(0, 0, Pixel.getBlue(img[i][j]));
					} break;
				default: System.err.println("Invalid color channel provided. Provide 'r', 'g', or 'b'.");
				}
			}
		}
		return slice;
	}
	
	/**
	 * Produces the color distribution of an image by splitting the RGB
	 * values in ranges of 16.
	 * 
	 * @param img - input image
	 * @param channel - color channel: either 'r', 'g', or 'b'
	 * @param thresh - minimum threshold value for the RGB channel 
	 * @return - the color distribution
	 */
	public static int[] colorDistrib(int[][] img, char channel, int thresh){
		int[] crRed = new int[16];
		int[] crGreen = new int[16];
		int[] crBlue = new int[16];
		for(int i = 0; i < img.length; i++){
			for(int j = 0; j < img[0].length; j++){
				switch (channel){
				case 'r': 
					int red = Pixel.getRed(img[i][j]);
					if(red >= thresh) {
						crRed[red / 16]++;
					} break;
				case 'g':
					int green = Pixel.getGreen(img[i][j]);
					if(green >= thresh) {
						crGreen[green / 16]++;
					} break;
				case 'b': 
					int blue = Pixel.getBlue(img[i][j]);
					if(blue >= thresh) {
						crBlue[blue / 16]++;
					} break;
				default: System.err.println("Ivalid color channel provided. Provide 'r', 'g', or 'b'.");
				}
			}
		}
		//System.out.println(Arrays.toString(crRed));
		//System.out.println(Arrays.toString(crGreen));
		//System.out.println(Arrays.toString(crBlue));
		switch (channel){
		case 'r': return dominant(crRed);
		case 'g': return dominant(crGreen);
		case 'b': return dominant(crBlue);
		default: return null;
		}
	}
	
	/**
	 * Constructs an array that contains the lower bounds for the dominant color
	 * values of the channels in an image.
	 * 
	 * @param density - the density distribution as computed by colorDistrib
	 * @return the dominant ranges
	 */
	private static int[] dominant(int[] density){
		PriorityQueue<Pair<Integer, Integer>> pq = 
				new PriorityQueue<Pair<Integer, Integer>>(density.length, Collections.reverseOrder());
		for(int i = 0; i < density.length; i++){
			Pair<Integer, Integer> p = new Pair<Integer, Integer>(i, density[i]);
			pq.add(p);
		}
		ArrayList<Integer> al = new ArrayList<Integer>();
		int ratio = 256/density.length;
		while(!pq.isEmpty()){
			Pair<Integer, Integer> p = pq.remove();
			if(p.getValue() == 0) break;
			al.add(p.getKey()*ratio);
		}
		int[] dominant = new int[al.size()];
		int index = 0;
		for(Iterator<Integer> it = al.iterator(); it.hasNext(); ){
			dominant[index] = it.next();
			index++;
		}
		return dominant;
	}
	
	
	/**
	 * Produces the layer cuts based on the distribution of the colors.
	 * 
	 * @param slice - the RGB slice
	 * @param distrib - the color distribution
	 * @param channel - either r/g/b value
	 */
	public static List<Layer> layers(int[][] slice, int[] distrib, char channel){
		LinkedList<Layer> layers = new LinkedList<Layer>();
		for(int i = 0; i < distrib.length; i++){
			int[][] layer = cutLayer(slice, distrib[i], channel);
			double[][] layerDensity = layerDensity(layer, BLOCK_SIZE);
			if(layerDensity != null){
				Layer l = new Layer(layer, layerDensity, new Pair<Integer, Integer>(distrib[i], distrib[i]+16), i);
				layers.add(l);
			}
			BufferedImage bi = Image.makeImage(layer);
			Image.write(bi, "layers\\"+i+".jpg");
		}
		return layers;
	}
	
	/**
	 * Splits the image (layer) by the block size given by factor. Then it
	 * performs density analysis to see which parts of the image actually contain
	 * solid objects.
	 * 
	 * @param layer - the input image (layer)
	 * @param factor - the block size
	 * @return - if the maxDensity is less than DENSITY_THRESH then null,
	 * 	otherwise it returns the density array
	 */
	private static double[][] layerDensity(int[][] layer, double factor) {
		int x = (int) Math.ceil(layer.length/factor);
		int y = (int) Math.ceil(layer[0].length/factor);
		double[][] density = new double[x][y];
		double maxDensity = 0;
		int maxX = 0;
		int maxY = 0;
		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				// Extract the density for a block
				int xMin = Math.min(layer.length, (i+1)*(int)factor);
				int yMin = Math.min(layer[0].length, (j+1)*(int)factor);
				double colorPixels = 0.0;
				double area = factor*factor;
				for(int ii = i*(int)factor; ii < xMin; ii++){
					for(int jj = j*(int)factor; jj < yMin; jj++){
						if(!Pixel.isWhite(layer[ii][jj]))
							colorPixels++;
					}
				}
				density[i][j] = colorPixels/area;
				if(density[i][j] > maxDensity){
					maxDensity = density[i][j];
					maxX = i;
					maxY = j;
				}
			}
		}
		//System.out.println("Maximum density: " + maxDensity + " at ("+maxX+", "+maxY+")");
		if(maxDensity < DENSITY_THRESH)
			return null;
		return density;
	}

	/**
	 * Makes one layer per a color lower bound provided
	 * 
	 * @param slice - the original RGB slice
	 * @param colorLowerBound - the color lower bound value
	 * @param channel - either r/g/b value
	 * @return the layer cut
	 */
	private static int[][] cutLayer(int[][] slice, int colorLowerBound, char channel){
		int[][] layer = new int[slice.length][slice[0].length];
		int white = Pixel.create(255, 255, 255);
		for(int i = 0; i < slice.length; i++){
			for(int j = 0; j < slice[0].length; j++){
				layer[i][j] = white;
				switch (channel){
				case 'r': 
					int red = Pixel.getRed(slice[i][j]);
					if(red >= colorLowerBound && red < (colorLowerBound + 16)){
						layer[i][j] = slice[i][j];
					} break;
				case 'g': 
					int green = Pixel.getGreen(slice[i][j]);
					if(green >= colorLowerBound && green < (colorLowerBound + 16)){
						layer[i][j] = slice[i][j];
					} break;
				case 'b': 
					int blue = Pixel.getBlue(slice[i][j]);
					if(blue >= colorLowerBound && blue < (colorLowerBound + 16)){
						layer[i][j] = slice[i][j];
					} break;
				default: System.err.println("Channel is not r/g/b when it should be");
				}
			}
		}
		return layer;
	}

}
