package seniordesign.xca;

import java.util.Iterator;
import java.util.List;

import org.jfugue.Pattern;

import seniordesign.Composable;
import seniordesign.Image;
import seniordesign.chromaticanalysis.Chroma;


public class ExtendedCA implements Composable {

	@Override
	public Pattern compose(Image img) {
		int[][] pixels = img.getPixels();
		
		int[][] red = HistogramSlicer.slice(pixels, 'r', img.avgRed());
		//HistogramSlicer.slice(pixels, 'r', img.avgGreen());
		//HistogramSlicer.slice(pixels, 'r', img.avgBlue());
		
		//HistogramSlicer.slice(pixels, 'g', img.avgRed());
		int[][] green = HistogramSlicer.slice(pixels, 'g', img.avgGreen());
		//HistogramSlicer.slice(pixels, 'g', img.avgBlue());
		
		//HistogramSlicer.slice(pixels, 'b', img.avgRed());
		//HistogramSlicer.slice(pixels, 'b', img.avgGreen());
		int[][] blue = HistogramSlicer.slice(pixels, 'b', img.avgBlue());
		
		//BufferedImage bi = Image.makeImage(slice);
		//Image.write(bi, "slices\\"+channel+height+".jpg");
		int[] cdRed = HistogramSlicer.colorDistrib(red, 'r', img.avgRed());
		int[] cdGreen = HistogramSlicer.colorDistrib(green, 'g', img.avgGreen());
		int[] cdBlue = HistogramSlicer.colorDistrib(blue, 'b', img.avgBlue());
		
		//System.out.println(Arrays.toString(cdRed));
		//System.out.println();
		//int[] cdPixels = HistogramSlicer.colorDistrib(pixels, 'r', 0);
		//System.out.println(Arrays.toString(cdPixels));
		
		//System.out.println(Arrays.toString(cdGreen));
		//System.out.println();
		//int[] cdPixels = HistogramSlicer.colorDistrib(pixels, 'g', 0);
		//System.out.println(Arrays.toString(cdPixels));
		
		// the histogram layer slices of the RGB values
		List<Layer> redLayers = HistogramSlicer.layers(red, cdRed, 'r');
		List<Layer> greenLayers = HistogramSlicer.layers(green, cdGreen, 'g');
		List<Layer> blueLayers = HistogramSlicer.layers(blue, cdBlue, 'b');
		
		// a new Chromatic Graph with the image specified
		ChromaticGraph g = new ChromaticGraph(pixels);
		
		// three productions of Chromatic Vertices created from the Layers
		List<ChromaticVertex> redv = g.fromLayers(redLayers);
		List<ChromaticVertex> greenv = g.fromLayers(greenLayers);
		List<ChromaticVertex> bluev = g.fromLayers(blueLayers);
		
		g.add(redv).add(greenv).add(bluev);
		g.shrink();
		List<ChromaticVertex> vertices = g.getVertices();
		System.out.println(vertices);
		
		
		String chords = new String();
		chords = "I[" + Chroma.getInstrument(img.avgPixel()) + "] ";
		
		// Create the melody
		for(Iterator<ChromaticVertex> it = vertices.iterator(); it.hasNext(); ){
			ChromaticVertex cv = it.next();
			String crd = cv.toChord();
			chords += " Rq " + crd;
		}
		
		Pattern patt = new Pattern(chords);
		
		return patt;
	}

}
