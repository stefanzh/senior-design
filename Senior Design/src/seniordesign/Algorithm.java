package seniordesign;

/**
 * Enumeration of all the algorithms used in the project.
 * @author user
 *
 */
public enum Algorithm {

	DIRECT_MAPPING,
	CHROMATIC_ANALYSIS,
	xCA;
	
}
