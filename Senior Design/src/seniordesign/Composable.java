package seniordesign;

import org.jfugue.Pattern;

/**
 * This interface restricts every algorithm to contain a method compose()
 * which creates a jFugue Pattern that can be played.
 * 
 * @author Stefan Zhelyazkov stz@seas.upenn.edu
 * @author user
 * @author user
 *
 */
public interface Composable {
	
	public Pattern compose(Image img);

}
