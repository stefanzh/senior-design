package seniordesign.xca;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import seniordesign.Pair;
import seniordesign.chromaticanalysis.Chroma;

public class ChromaticGraph {
	
	/* Pixels of the original image */
	private int[][] img;
	private List<ChromaticVertex> vertices;
	
	/**
	 * Constructs a graph with a 2D array of the original image.
	 */
	public ChromaticGraph(int[][] img){
		this.img = img;
		this.vertices = new LinkedList<ChromaticVertex>();
	}
	
	/**
	 * Produces a list of Chromatic Vertices in a sorted by area order.
	 * 
	 * @param layers - the layers of a given RGB slice
	 * @return a sorted list of Chromatic Vertices
	 */
	public List<ChromaticVertex> fromLayers(List<Layer> layers){
		// produce the list
		List<ChromaticVertex> list = new LinkedList<ChromaticVertex>();
		for(Iterator<Layer> it = layers.iterator(); it.hasNext(); ){
			Layer next = it.next();
			Pair<Integer, Integer> avgColorArea = next.avgColor(img);
			Chroma c = Chroma.closestChroma(avgColorArea.getKey());
			int area = avgColorArea.getValue();
			ChromaticVertex cv = new ChromaticVertex(c, area);
			list.add(cv);
		}
		
		// sort the list and return it
		Collections.sort(list);
		return list;
	}
	
	/**
	 * Combines the current list of vertices with the provided list of vertices.
	 * 
	 * @param vertices - the list of vertices the graph will be combined with
	 * @return the modified graph
	 */
	public ChromaticGraph add(List<ChromaticVertex> vertices){
		this.vertices.addAll(vertices);
		Collections.sort(this.vertices);
		System.out.println("After addAll: " + this.vertices);
		return this;
	}
	
	/**
	 * Shrinks the list of Chromatic Vertices by combining 
	 * similar adjacent vertices.
	 */
	public void shrink(){
		List<ChromaticVertex> nl = new LinkedList<ChromaticVertex>();
		ChromaticVertex current = null;
		for(Iterator<ChromaticVertex> it = this.vertices.iterator(); it.hasNext(); ){
			ChromaticVertex cv = it.next();
			if (current == null) {
				current = cv;
				continue;
			}
			// if the two chromas are equal, combine the vertices
			if(cv.getChroma().equals(current.getChroma())){
				current = current.combine(cv);
			}
			else {
				nl.add(current);
				current = cv;
			}
		}
		if(current != null) nl.add(current);
		this.vertices = nl;
	}
	
	/**
	 * Returns the list of Chromatic Vertices
	 */
	public List<ChromaticVertex> getVertices(){
		return this.vertices;
	}

}
