package seniordesign.chromaticanalysis;

import java.util.LinkedList;
import org.jfugue.*;

import seniordesign.Composable;
import seniordesign.Image;

public class ChromaticAnalysis implements Composable {
	
	private LinkedList<CBrick> wall = new LinkedList<CBrick>();
	
	/**
	 * Creates a chromatic wall based on the threshold parameters
	 * defined in Chroma.java by constructing and adding bricks to it.
	 * Then each brick from the chromatic wall is passed as a parameter
	 * to a static function in BrickToMusic class which produces a music
	 * pattern for it. The final music pattern is returned.
	 * 
	 * @return jFugue music pattern created from the chromatic wall
	 */
	public Pattern compose(Image img){
		int[][] pixels = img.getPixels();
		CBrick brick = new CBrick();
		for(int i = 0; i < pixels.length; i++){
			for(int j = 0; j < pixels[0].length; j++){
				int rgb = pixels[i][j];
				Chroma c = Chroma.closestChroma(rgb);
				
				// if we can't add to the brick anymore,
				// it is complete
				if(!brick.add(c)){
					wall.add(brick);
					brick = new CBrick();
					brick.add(c);
				}
			}
		}
		
		Pattern full = new Pattern();
		for(CBrick cb : wall){
			full.add(BrickToMusic.convertToPattern(cb));
		}
		return full;
	}

}
