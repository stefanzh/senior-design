package seniordesign.directmapping;

import org.jfugue.*;

import seniordesign.Composable;
import seniordesign.Image;
import seniordesign.Pixel;

/**
 * Implements the Direct-mapping algorithm for producing music from images.
 * 
 * @author Stefan Zhelyazkov stz@seas.upenn.edu
 * @author user
 * @author user
 *
 */

public class DirectMapping implements Composable {
	
	/**
	 * Composes a musical piece through DirectMapping of the pixels 
	 * in the image.
	 * Takes average 
	 * @param pixels: the pixels of the input image
	 * @return a pattern containing the song
	 */
	public Pattern compose(Image img){
		int[][] pixels = img.getPixels();
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < pixels.length; i++){
			int j = 0;
			for(; j < pixels[0].length; j++){
				int red = Pixel.getRed(pixels[i][j]);
				int green = Pixel.getGreen(pixels[i][j]);
				int blue = Pixel.getBlue(pixels[i][j]);
				int base = (red + green + blue) % 25;
				sb.append("[" + (44 + base) + "]s ");
			}
			if(i*j > 120) break;
		}
		Pattern dm = new Pattern(sb.toString());
		return dm;
	}

}
