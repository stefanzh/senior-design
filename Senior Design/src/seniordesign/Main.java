package seniordesign;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.jfugue.Pattern;
import org.jfugue.Player;

import seniordesign.chromaticanalysis.ChromaticAnalysis;
import seniordesign.directmapping.DirectMapping;
import seniordesign.xca.ChordProgression;
import seniordesign.xca.ExtendedCA;


/**
 * The main class that runs the program.
 * 
 * @author user
 * @author user
 * @author user
 *
 */

public class Main {
	
	private static final Algorithm a = Algorithm.xCA;
	private static Composable composer = null;
	
	/**
	 * Main method
	 * @param args: image file to read from
	 */
	public static void main(String[] args) {
		if(args.length > 0){
			// read the image
			Image img = new Image(args[0]);
			
			String filename = args[0];
			//System.out.println(filename);
			String noextension = filename.split("\\.")[0];
			//System.out.println(noextension);
			
			// save a smaller copy of the image
			int[][] pixels = img.scale(400, 400);
			BufferedImage resized = Image.makeImage(pixels);
			Image.write(resized, "resized.jpg");
			
			// assign the algorithm constructor to the composer
			switch (a){
				case DIRECT_MAPPING: composer = new DirectMapping(); break;
				case CHROMATIC_ANALYSIS: composer = new ChromaticAnalysis(); break;
				case xCA: composer = new ExtendedCA(); break;
				default: System.err.println("Error with choosing the algorithm");
			}
			
			//ChordProgression pg = new ChordProgression();
			//String[] mainProgression = pg.mainProgression(pg.energetic, "C", false, "maj");
			//String[][] relatedProgressions = pg.relatedProgressions(pg.energetic, "C", "maj");
			
			// compose the music
			Pattern song = composer.compose(img);
			//Pattern song = new Pattern("A#majq Cminq Dminq D#majq Fmajq Gminq Adimq");
			//Pattern song = new Pattern("X[Volume]=16200 Cq Aminq Fq Gq Cq Aminq Fq Gq");
			System.out.println(song);
			// Play the song!
			Player player = new Player();
			try {
				player.saveMidi(song, new File(noextension+".midi"));
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			player.play(song);
		}
		else {
			System.out.println("You need to provide an argument to the program.");
		}
	}

}
