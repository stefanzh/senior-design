package seniordesign.chromaticanalysis;

import java.util.Collection;
import java.util.LinkedList;

import seniordesign.Pixel;


/**
 * Enumeration of all the chroma types along with their
 * chromatic indices and RGB values.
 * 
 * @author Stefan Zhelyazkov stz@seas.upenn.edu
 * @author user
 * @author user
 *
 */
public class Chroma {
	private final static Chroma WHITE = new Chroma(1.0, 223, 223, 223, "WHITE");
	private final static Chroma TURQUOISE = new Chroma(1.1, 0, 192, 192, "TURQUOISE");
	private final static Chroma GREEN = new Chroma(1.2, 64, 128, 64, "GREEN"); 
	private final static Chroma YELLOW = new Chroma(1.3, 192, 192, 0, "YELLOW");
	private final static Chroma ORANGE = new Chroma(1.4, 192, 128, 0, "ORANGE");
	private final static Chroma RED = new Chroma(1.5, 128, 32, 64, "RED"); 
	private final static Chroma PINK = new Chroma(1.6, 225, 128, 225, "PINK"); 
	private final static Chroma BLUE = new Chroma(1.7, 32, 64, 128, "BLUE"); 
	private final static Chroma PURPLE = new Chroma(1.8, 192, 64, 192, "PURPLE"); 
	private final static Chroma BROWN = new Chroma(1.9, 96, 64, 0, "BROWN"); 
	private final static Chroma GRAY = new Chroma(2.0, 128, 128, 128, "GRAY");
	private final static Chroma BLACK = new Chroma(2.1, 32, 32, 32, "BLACK");
	
	private final double x;
	private final int r;
	private final int g;
	private final int b;
	private final String name;
	private static Collection<Chroma> basicColors = Chroma.values();
	
	/**
	 * Constructor for Chroma.
	 * 
	 * @param x = the chromatic index
	 * @param r = the red value (0; 255)
	 * @param g = the green value (0; 255)
	 * @param b = the blue value (0; 255)
	 */
	Chroma(double x, int r, int g, int b, String name){
		this.x = x;
		this.r = r;
		this.g = g;
		this.b = b;
		this.name = name;
	}
	
	/**
	 * Computes the closest Chroma matching and then retrieves the chromatic index.
	 * 
	 * The chromatic index = the index of the closes chroma + 0.1*diff/gap.
	 * diff = sum of the absolute differences of the R, G, B values
	 * gap = 128 is the relative gap between chromatic indices
	 * the coefficient = 0.1 because that is the difference unit between chromatic indices
	 * 
	 * The ratio diff/gap will always add to the closest chromatic index because values
	 * are computed in the range 1.0 to 2.1, and we don't want to get values below
	 * or above those boundaries (ensured by this addition of diff/gap).
	 * 
	 * @param rgb is the pixel
	 * @return a double of the closest chromatic index
	 */
	public static Chroma closestChroma(int rgb){
		int red = Pixel.getRed(rgb);
		int green = Pixel.getGreen(rgb);
		int blue = Pixel.getBlue(rgb);
		
		Chroma closest = WHITE;
		int difference = Integer.MAX_VALUE;
		
		for(Chroma c : basicColors){
			int diff = 0;
			diff += Math.abs(c.r - red);
			diff += Math.abs(c.g - green);
			diff += Math.abs(c.b - blue);
			if (difference > diff){
				difference = diff;
				closest = c;
			}
		}
		
		double ci = Math.min(closest.x + 0.1*difference/128, BLACK.x);
		return new Chroma(ci, red, green, blue, closest.name);
	}
	
	/**
	 * Creates the collection of the basic colors.
	 * @return the collection of the basic colors.
	 */
	private static Collection<Chroma> values(){
		Collection<Chroma> c = new LinkedList<Chroma>();
		c.add(WHITE);
		c.add(TURQUOISE);
		c.add(GREEN);
		c.add(YELLOW);
		c.add(ORANGE);
		c.add(RED);
		c.add(PINK);
		c.add(BLUE);
		c.add(PURPLE);
		c.add(BROWN);
		c.add(GRAY);
		c.add(BLACK);
		return c;
	}
	
	
	/**
	 * Overrides default toString().
	 */
	public String toString(){
		return name + " (" + x + ", " + r + ", " + g + ", " + b + ")";
	}
	
	/**
	 * Overrides the default equals()
	 */
	public boolean equals(Chroma oc){
		return this.name.equals(oc.name);
	}
	
	/**
	 * Combines the current Chroma and a new Chroma
	 * 
	 * @param oc - the other Chroma
	 * @return the combined Chromas
	 */
	public Chroma combine(Chroma oc){
		double nx = (x + oc.x)/2;
		int nr = (r + oc.r)/2;
		int ng = (g + oc.g)/2;
		int nb = (b + oc.b)/2;
		return new Chroma(nx, nr, ng, nb, this.name);
	}
	
	/**
	 * Based on the Chromatic Index of the closest Chroma, 
	 * this method decides which instrument to play. 
	 * 
	 * @return the string of the instrument to play.
	 */
	public static String getInstrument(int pixel){
		Chroma closest = closestChroma(pixel);
		if(closest.x < 1.1) return "Piano"; // if `white` return Piano
		if(closest.x < 1.2) return "Clarinet"; // if `turquoise`
		if(closest.x < 1.3) return "Xylophone"; // if `green`
		if(closest.x < 1.4) return "Trumpet"; // if `yellow`
		if(closest.x < 1.5) return "Marimba"; // if `orange`
		if(closest.x < 1.6) return "Guitar"; // if `red`
		if(closest.x < 1.7) return "Viola"; // if `pink`
		if(closest.x < 1.8) return "Violin"; // if `blue`
		if(closest.x < 1.9) return "Guitar"; // if `purple`
		if(closest.x < 2.0) return "Guitar"; // if `brown`
		if(closest.x < 2.1) return "Accordian"; // if `gray`
		return "Piano"; // if `black`
	}
	
	/**
	 * Returns the chromatic index of the chroma.
	 * @return the chromatic index of the chroma.
	 */
	public double getCI(){
		return x;
	}

}
