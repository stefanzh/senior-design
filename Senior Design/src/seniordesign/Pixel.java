package seniordesign;

/**
 * Pixel
 * 
 * Pixel is a simple class that encapsulates the human understanding of a pixel. 
 * Each instance of this class holds three values that represent the red color,
 * green color and blue color values of a pixel.
 * 
 * Pixels are integers of the form 0xRRGGBB where 
 * RR = 0xRR0000
 * GG = 0x00GG00
 * BB = 0x0000BB
 */

public class Pixel {
	
	/**
	 * Get the red value of the pixel
	 * @return the red value of the pixel
	 */
	public static int getRed(int pixel){
		return pixel >> 16 & 0xFF;
	}
	
	/**
	 * Get the green value of the pixel
	 * @return the green value of the pixel
	 */
	public static int getGreen(int pixel){
		return pixel >> 8 & 0xFF;
	}
	
	/**
	 * Get the blue value of the pixel
	 * @return the blue value of the pixel
	 */
	public static int getBlue(int pixel){
		return pixel & 0xFF;
	}
	
	/**
	 * Checks whether a given pixel is black.
	 * 
	 * @param pixel - the pixel being checked
	 * @return if it's black true, false otherwise
	 */
	public static boolean isBlack(int pixel){
		return 0 == pixel;
	}
	
	/**
	 * Checks whether a given pixel is white.
	 * 
	 * @param pixel - the pixel being checked
	 * @return if it's white true, false otherwise
	 */
	public static boolean isWhite(int pixel){
		return ((255 << 16) | (255 << 8) | 255) == pixel;
	}
	
	/**
	 * Creates a pixels from provided RGB values.
	 * 
	 * @param r - red component
	 * @param g - green component
	 * @param b - blue component
	 * @return a pixel with the specified RGB components
	 */
	public static int create(int r, int g, int b){
		return (r << 16) | (g << 8) | b;
	}
	
	/**
	 * Converts the given pixel to a String.
	 * 
	 * @param pixel - integer value of the pixel
	 * @return - the pretty-printed value of the pixel
	 */
	public static String toString(int pixel){
		return "(" + getRed(pixel) + "," + getGreen(pixel) + "," + getBlue(pixel) + ")";
	}
}
